# -*- coding: utf-8 -*-
# Copyright (C) 2017 Chloe Chamaillard, Delphine Delorme, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import StealEvent

class StealAction(Action2):
    EVENT = StealEvent
    RESOLVE_OBJECT = "resolve_for_take"
    ACTION = "steal"
