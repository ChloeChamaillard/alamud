# -*- coding: utf-8 -*-
# Copyright (C) 2017 Chloe Chamaillard, Delphine Delorme, IUT d'Orléans
#==============================================================================

from .effect import Effect2, Effect3
from mud.events import KillEvent, KillWithEvent

class KillEffect(Effect2):
    EVENT = KillEvent

class KillWithEffect(Effect3):
    EVENT = KillWithEvent
