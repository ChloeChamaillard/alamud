# -*- coding: utf-8 -*-
# Copyright (C) 2017 Chloe Chamaillard, Delphine Delorme, IUT d'Orléans
#==============================================================================

from .effect import Effect2
from mud.events import StealEvent

class StealEffect(Effect2):
    EVENT = StealEvent
