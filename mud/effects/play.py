# -*- coding: utf-8 -*-
# Copyright (C) 2017 Chloe Chamaillard, Delphine Delorme, IUT d'Orléans
#==============================================================================

from .effect import Effect2
from mud.events import PlayEvent

class PlayEffect(Effect2):
    EVENT = PlayEvent
