# -*- coding: utf-8 -*-
# Copyright (C) 2017 Chloe Chamaillard, Delphine Delorme, IUT d'Orléans
#==============================================================================

from .event import Event2

class StealEvent(Event2):
    NAME = "steal"

    def perform(self):
        if not self.object.has_prop("stealable"):
            self.add_prop("object-not-stealable")
            return self.steal_failed()
        if self.object in self.actor:
            self.add_prop("object-already-in-inventory")
            return self.steal_failed()
        self.object.move_to(self.actor)
        self.inform("steal")

    def steal_failed(self):
        self.fail()
        self.inform("steal.failed")
